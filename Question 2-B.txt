b) Selon-vous en quoi la méthode de travail SCRUM (sprints) est-elle avantageuse ?
Nommez et expliquez au moins deux avantages qu’elle présente. (15%)

Réponse:

Avantage 1): La méthode Agile(SCRUM) permet la flexibilité et implémentations des différents contenus d'un projet informatique tout au long de son développement.
Contrairement à la gestion de projet ordinaire qui se rend du point A au point Z, la méthode Agile permet de passer du point A au Point B en en incrémentant à chaque palier
pour revoir ce qui aurait pu être mieux fait.



Avantage 2): Elle permet d'obtenir un résultat beaucoup plus satisfaisant pour le client, car il pourra communiqué et expliqué sa vision
du produit tout au long du projet.





